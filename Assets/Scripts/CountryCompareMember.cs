﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountryCompareMember : MonoBehaviour
{
    [SerializeField] private Text _countryText;
    [SerializeField] private Text _gdpText;
    [SerializeField] private Text _areaText;
    [SerializeField] private Text _populationText;
    public CountryInfo _info;
 
  
    public void Init(CountryInfo info)
    {
        _info = info;
        _countryText.text = info.CountryName;
        _gdpText.text = info.Gdp.ToString();
        _areaText.text = info.Area.ToString();
        _populationText.text = info.Population.ToString();
    }
}
