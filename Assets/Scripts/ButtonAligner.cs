﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonAligner : MonoBehaviour
{
   

    [SerializeField] private GameObject _alignGO;

    void Update()
    {
        transform.position = Camera.main.WorldToScreenPoint(_alignGO.transform.position);
    }
}

