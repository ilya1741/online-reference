﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public static GameController Instance;

    [SerializeField] private UIView _uiView;

    [SerializeField] GameObject _manyCountriesSelectedWindow;

    [SerializeField] Text _countSelectedCountriesText;


    public List<CountryInfo> CurrentSelectedCountriesInfo
    {
        get { return _currentSelectedCountriesInfo; }
    }

    private List<CountryInfo> _currentSelectedCountriesInfo = new List<CountryInfo>();
    private List<Country> _currentSelectedCountries = new List<Country>();

    public void CleanAllInfo()
    {
        _currentSelectedCountriesInfo.Clear();
        foreach (var country in _currentSelectedCountries)
        {
            country.OnDeselected(false);
        }
        _currentSelectedCountries.Clear();
        _uiView._clearList.gameObject.SetActive(false);
        CheckHowManyCountriesWasSelected();

    }


    public void AddCountryToSelectedList(CountryInfo info, Country country)
    {
        _currentSelectedCountriesInfo.Add(info);
        _currentSelectedCountries.Add(country);
        _uiView._clearList.gameObject.SetActive(true);
        CheckHowManyCountriesWasSelected();
    }

    public void RemoveCountryToSelectedList(CountryInfo info, Country country)
    {
        _currentSelectedCountriesInfo.Remove(info);
        _currentSelectedCountries.Remove(country);
        if (_currentSelectedCountries.Count == 0)
        {
            _uiView._clearList.gameObject.SetActive(false);
        }

        CheckHowManyCountriesWasSelected();
    }


    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    void OnDestroy()
    {
        Instance = null;
    }

    void CheckHowManyCountriesWasSelected()
    {
        _manyCountriesSelectedWindow.SetActive(_currentSelectedCountriesInfo.Count > 0);
        if (_currentSelectedCountriesInfo.Count > 0)
        {
            _countSelectedCountriesText.text = "Выделено " + _currentSelectedCountriesInfo.Count + " стран";

        }
        else
        {
            _countSelectedCountriesText.text = "";
        }
    }
}

[Serializable]
public class CountryInfo
{
    public string CountryName;
    public double Gdp;
    public double Population;
    public double Area;

}



