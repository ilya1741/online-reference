﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float Speed;

    private Vector3 StartPos;
    private Camera cam;
    private float targetPosY;
    private float targetPosX;


    void Start()
    {
        cam = GetComponent<Camera>();
        targetPosY = transform.position.x;
        targetPosX = transform.position.y;
    }


    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            StartPos = cam.ScreenToViewportPoint(Input.mousePosition);

        }


        else if (Input.GetMouseButton(0))
        {

            float posY = cam.ScreenToViewportPoint(Input.mousePosition).y - StartPos.y;
            targetPosY = Mathf.Clamp(transform.position.x - posY, -22.79f, 6.06f);


            float posX = cam.ScreenToViewportPoint(Input.mousePosition).x - StartPos.x;
            targetPosX = Mathf.Clamp(transform.position.z + posX, -15.12f,12.4f );


        }
        transform.position = new Vector3(Mathf.Lerp(transform.position.x, targetPosY, Speed * Time.deltaTime),
            transform.position.y,
            Mathf.Lerp(transform.position.z, targetPosX, Speed * Time.deltaTime));



    }
}
