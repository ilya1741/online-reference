﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class SelectedCountryListWindow : MonoBehaviour
{
    [SerializeField] private Button _sortingAreaButton;
    [SerializeField] private Button _sortingGdpButton;
    [SerializeField] private Button _sortingPopulationButton;
    [SerializeField] private Button _backButton;

    [SerializeField] private GameObject _parentforSpawn;
    [SerializeField] private CountryCompareMember _countryMember;

    private List<CountryCompareMember> _members = new List<CountryCompareMember>();

    private bool _ascendingPopulation;
    private bool _ascendingGdp;
    private bool _ascendingArea;



    void Awake()
    {
        _sortingAreaButton.onClick.AddListener(SortingArea);

        _sortingPopulationButton.onClick.AddListener(SortingPopulation);

        _sortingGdpButton.onClick.AddListener(SortingGdp);

        _backButton.onClick.AddListener(Back);
    }

    void OnEnable()
    {
        foreach (var country in GameController.Instance.CurrentSelectedCountriesInfo)
        {
            var member = Instantiate(_countryMember, _parentforSpawn.transform);
            member.Init(country);
            _members.Add(member);
        }
    }

    void OnDestroy()
    {
        _sortingAreaButton.onClick.RemoveListener(SortingArea);
        _sortingGdpButton.onClick.RemoveListener(SortingGdp);
        _sortingPopulationButton.onClick.RemoveListener(SortingPopulation);
        _backButton.onClick.RemoveListener(Back);

    }

    void Back()
    {
        gameObject.SetActive(false);
    }
    
    void SortingArea()
    {
        if (_ascendingArea)
        {
            var sortedMembers = from member in _members
                                orderby member._info.Area ascending
                                select member;

            _members = sortedMembers.ToList();
        }
        else
        {
            var sortedMembers = from member in _members
                                orderby member._info.Area descending
                                select member;

            _members = sortedMembers.ToList();
        }

        for (int i = 0; i < _members.Count; i++)
        {
            _members[i].transform.SetSiblingIndex(i);
        }

        _ascendingArea = !_ascendingArea;

    }
    
    void SortingGdp()
    {
        if (_ascendingGdp)
        {
            var sortedMembers = from member in _members
                                orderby member._info.Gdp ascending
                                select member;

            _members = sortedMembers.ToList();
        }
        else
        {
            var sortedMembers = from member in _members
                                orderby member._info.Gdp descending
                                select member;

            _members = sortedMembers.ToList();
        }

        for (int i = 0; i < _members.Count; i++)
        {
            _members[i].transform.SetSiblingIndex(i);
        }

        _ascendingGdp = !_ascendingGdp;
    }
    
    void SortingPopulation()
    {

        if (_ascendingPopulation)
        {
            var sortedMembers = from member in _members
                                orderby member._info.Population ascending
                                select member;

            _members = sortedMembers.ToList();
        }
        else
        {
            var sortedMembers = from member in _members
                                orderby member._info.Population descending
                                select member;

            _members = sortedMembers.ToList();
        }

        for (int i = 0; i < _members.Count; i++)
        {
            _members[i].transform.SetSiblingIndex(i);
        }

        _ascendingPopulation = !_ascendingPopulation;
    }
}
