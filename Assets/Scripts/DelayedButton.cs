﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DelayedButton : Button
{
    public UnityEvent OnSelectedEvent = new UnityEvent();

    public UnityEvent OnDeselectedEvent = new UnityEvent();

    public  delegate void GlobalOnClick();

    public static event GlobalOnClick OnClick;

    const float _delay = 1f;

    Coroutine _waitRoutine;

    ButtonState _state = ButtonState.Idle;

    public override void OnPointerDown(PointerEventData eventData)
    {
        OnClick();
        base.OnPointerDown(eventData);

        if (_state == ButtonState.Selected)
            OnDeselectedEvent?.Invoke();

        _state = ButtonState.Idle;

        _waitRoutine = StartCoroutine(WaitRoutine());
    }

    public override void OnPointerUp(PointerEventData eventData)
    {
        if (_waitRoutine != null)
        {
            StopCoroutine(_waitRoutine);
            _waitRoutine = null;
        }
        base.OnPointerUp(eventData);
    }

    IEnumerator WaitRoutine()
    {
        yield return new WaitForSeconds(_delay);
        _state = ButtonState.Selected;
        OnSelectedEvent.Invoke();
        _waitRoutine = null;
    }

    enum ButtonState
    {
        Idle,
        Selected
    }
}