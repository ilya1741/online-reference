﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShortinfoWindow : MonoBehaviour
{
   

    [SerializeField] private Text _countryText;
    [SerializeField] private Text _gdpText;
    [SerializeField] private Text _areaText;
    [SerializeField] private Text _populationText;

    public static ShortinfoWindow Instance;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    void OnDestroy()
    {
          Instance = null;
    }

  
    public void Init(CountryInfo info)
    {
        _countryText.text = info.CountryName;
        _gdpText.text = info.Gdp.ToString();
        _areaText.text = info.Area.ToString();
        _populationText.text = info.Population.ToString();
    }
}
