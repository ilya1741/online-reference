﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Country : MonoBehaviour
{


    [SerializeField] private GameObject _pointOfInterest;
    [SerializeField] private GameObject _geotag;
    [SerializeField] private GameObject _checkMark;

    [SerializeField] private CountryInfo _info;

    [SerializeField] private DelayedButton _delayedButton;

    private bool _isIgnoreClick;
    private States _state;

    public enum States
    {
        Selected,
        Deselected,
        Clicked
    }

    void SetState(States state)
    {
        _state = state;
        _pointOfInterest.gameObject.SetActive(state == States.Clicked || state == States.Selected);
        _geotag.gameObject.SetActive(state == States.Deselected);
        _checkMark.gameObject.SetActive(state == States.Selected);
    }
    void OnSelected()
    {
        _isIgnoreClick = true;
        SetState(States.Selected);
        GameController.Instance.AddCountryToSelectedList(_info, this);
    }
    public void OnDeselected(bool remove = true)
    {
        _isIgnoreClick = false;
        SetState(States.Deselected);
        if (remove)
        {
            GameController.Instance.RemoveCountryToSelectedList(_info, this);
        }

    }

    void OnClick()
    {
        if (_isIgnoreClick)
        {
            return;
        }
        SetState(States.Clicked);
        ShortinfoWindow.Instance.Init(_info);
    }


    void Awake()
    {
        _delayedButton.onClick.AddListener(OnClick);

        _delayedButton.OnSelectedEvent.AddListener(OnSelected);

        _delayedButton.OnDeselectedEvent.AddListener(() =>
        {
            OnDeselected();
        });

        DelayedButton.OnClick += () =>
        {
            if (_state == States.Clicked)
            {
                SetState(States.Deselected);
            }
        };
    }

    void OnDestroy()
    {
        _delayedButton.onClick.RemoveListener(OnClick);

        _delayedButton.OnSelectedEvent.RemoveListener(OnSelected);

        _delayedButton.OnDeselectedEvent.RemoveListener(() =>
        {
            OnDeselected();
        });


    }
}
