﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIView : MonoBehaviour
{
    [SerializeField] public Button _clearList;
    [SerializeField] private Button _listOfSelectedCountriesButton;
    [SerializeField] private GameObject _listOfSelectedCountries;




    void Awake()
    {
        _clearList.onClick.AddListener(ClearList);
        _listOfSelectedCountriesButton.onClick.AddListener(SetActiveListOfSelectedCountries);
    }

    void OnDestroy()
    {
        _clearList.onClick.RemoveListener(ClearList);
        _listOfSelectedCountriesButton.onClick.AddListener(SetActiveListOfSelectedCountries);
    }


    private void ClearList()
    {
        GameController.Instance.CleanAllInfo();
    }

    private void SetActiveListOfSelectedCountries()
    {
        _listOfSelectedCountries.gameObject.SetActive(!_listOfSelectedCountries.activeInHierarchy);
    }


}
